$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();

    $('.carousel').carousel({
        interval:1500
    });
    var elementSelected = "defaultElement";

    $('#contacto').on("show.bs.modal", function(e){
        console.log("el modal esta mostrando.");
        $(e.relatedTarget).removeClass("btn-outline-success");
        $(e.relatedTarget).addClass("btn-primary");
        $(e.relatedTarget).prop("disabled", true);
        elementSelected = e.relatedTarget;
        console.log(e);
    });
    $('#contacto').on("shown.bs.modal", function(e){
        console.log("el modal se mostró.");
    });
    $('#contacto').on("hide.bs.modal", function(e){
        console.log("el modal se oculta.");
    });
    $('#contacto').on("hidden.bs.modal", function(e){
        console.log("el modal se ocultó.");
        $(elementSelected).prop("disabled", false);
        $(elementSelected).addClass("btn-outline-success");
        $(elementSelected).removeClass("btn-primary");
        
    });


});